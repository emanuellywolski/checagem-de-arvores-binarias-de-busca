#include <stdio.h>
#include <stdlib.h>

typedef struct NoArvore {
    int valor;
    struct NoArvore* esquerda;
    struct NoArvore* direita;
} NoArvore;

// Função auxiliar recursiva para verificar a BST
int verificarBSTRecursivo(NoArvore* no, int min, int max) {
    // Caso base: uma árvore vazia é uma BST
    if (no == NULL) {
        return 1;
    }

    // Verificar se o valor do nó está dentro dos limites (min, max)
    if (no->valor < min || no->valor > max) {
        return 0;
    }

    // Recursivamente verificar a subárvore esquerda e a subárvore direita
    return (verificarBSTRecursivo(no->esquerda, min, no->valor - 1) &&
            verificarBSTRecursivo(no->direita, no->valor + 1, max));
}

// Função principal para verificar se a árvore é uma BST
int verificarBST(NoArvore* arvore) {
    return verificarBSTRecursivo(arvore, INT_MIN, INT_MAX);
}

int main() {
    // Exemplo de árvore binária de busca (BST)
    NoArvore* raiz = malloc(sizeof(NoArvore));
    raiz->valor = 10;
    raiz->esquerda = malloc(sizeof(NoArvore));
    raiz->esquerda->valor = 5;
    raiz->esquerda->esquerda = NULL;
    raiz->esquerda->direita = NULL;
    raiz->direita = malloc(sizeof(NoArvore));
    raiz->direita->valor = 15;
    raiz->direita->esquerda = NULL;
    raiz->direita->direita = NULL;

    // Verificar se a árvore é uma BST
    if (verificarBST(raiz)) {
        printf("A árvore é uma BST.\n");
    } else {
        printf("A árvore não é uma BST.\n");
    }

    return 0;
}

